/*global require*/
"use strict";

var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var pug = require('gulp-pug');

var paths = {
  dist: './dist/',
  distImg: './dist/img',
  css: './dist/css',
  sass: './app/sass/*.scss',
  pug: './app/*.pug',
  img: './app/img/*',
  js: './app/js/**/*.js',
}

gulp.task('default', ['compileAll', 'watch'], function () {})

gulp.task('watch', ['browserSync'], function () {
  gulp.watch("./app/**/**/*", ['compileAll'])
  gulp.watch("./app/img/**/*", ['images'])
})

gulp.task('compileAll', ['sass', 'pug', 'js'], function () {
  browserSync.reload();
});

gulp.task('images', function(){
  return gulp.src(paths.img)
    .pipe(gulp.dest(paths.distImg))
})

// compilators
gulp.task('sass', function () {
  return gulp.src(paths.sass)
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest(paths.dist))
})
gulp.task('pug', function () {
  return gulp.src(paths.pug)
    .pipe(pug())
    .on('error', function (err) {
        process.stderr.write(err.message + '\n');
        this.emit('end');
      })
    .pipe(gulp.dest(paths.dist))
});
gulp.task('js', function () {
  return gulp.src(paths.js)
    .pipe(gulp.dest(paths.dist))
});
// ------ 

gulp.task('browserSync', function () {
  browserSync.init({
    server: {
      baseDir: paths.dist
    },
  })
})